name             'backup'
maintainer       'Joe Yates'
maintainer_email 'info@bidsnc.com'
license          'Apache 2.0'
description      'Manages backups'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

supports 'ubuntu'

