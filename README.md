chef-backup-cookbook
====================
A chef cookbook that uses the backup gem to configure backups launched by cron jobs

# Status

Currently, the resource only handles backing up postgresql databases.

# Configuration

Add dependency to your cookbook's metadata.rb:

```ruby
depends 'backup'
```

Set up the backup system:

```ruby
backup_setup 'opaweb' do
  user                   node['deploy']['user']
  group                  node['deploy']['user']
  directory              File.join('', 'home', node['deploy']['user'], 'backup')
  gpg_key                node['gpg']['public_key']
  gpg_recipient          node['gpg']['recipient']
end
```

This will install the necessary dependencies and create the directory for backups.

Define a backup resource:

```ruby
backup_database 'a database' do
  user                   node['deploy']['user']
  group                  node['deploy']['user']
  directory              File.join('', 'home', node['deploy']['user'], 'backup')
  database_name          node['database']['name']
  database_username      node['database']['username']
  database_password      node['database']['password']
  database_host          node['database']['host']
  database_encrypt_gpg   true
  compress               true
  s3_credentials(
    access_key:          node['s3']['access_key'],
    secret_key:          node['s3']['secret_key'],
    region:              node['s3']['region'],
    bucket:              node['s3']['bucket'],
    path:                node['s3']['path']
  )
  cron(
    minute:      '23',
    hour:        '03',
    mailto:      'user@example.com',
    path:        '/usr/local/bin:/usr/bin:/bin'
  )
end
```
