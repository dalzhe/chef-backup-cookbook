#
# Cookbook Name:: backup
# Resource:: s3_store
#
# Copyright 2015, Joe Yates

actions :create
default_action :create

attribute :directory, :kind_of => String, :required => true
attribute :user, :kind_of => String, :default => 'root'
attribute :group, :kind_of => String, :default => 'root'
attribute :s3_credentials, :kind_of => Hash, :required => true
attribute :cron, :kind_of => Hash
attribute :store, :kind_of => Array
attribute :exclude, :kind_of => Array, :default => []
attribute :retention, :kind_of => Integer, :default => 7
