#
# Cookbook Name:: backup
# Resource:: database
#
# Copyright 2013, Joe Yates

actions :create
default_action :create

attribute :directory, :kind_of => String, :required => true
attribute :user, :kind_of => String, :default => 'root'
attribute :group, :kind_of => String, :default => 'root'
attribute :gpg_key, :kind_of => String
attribute :gpg_recipient, :kind_of => String
