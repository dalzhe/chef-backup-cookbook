#
# Cookbook Name:: backup
# Resource:: database
#
# Copyright 2013-4, Joe Yates

actions :create
default_action :create

attribute :directory, :kind_of => String, :required => true
attribute :user, :kind_of => String, :default => 'root'
attribute :group, :kind_of => String, :default => 'root'
attribute :database_name, :kind_of => String, :required => true
attribute :database_username, :kind_of => String, :required => true
attribute :database_password, :kind_of => String
attribute :database_host, :kind_of => String
attribute :database_encrypt_gpg, :kind_of => [TrueClass, FalseClass], :default => false
attribute :compress, :kind_of => [TrueClass, FalseClass], :default => true
attribute :s3_credentials, :kind_of => Hash
attribute :cron, :kind_of => Hash
attribute :retention, :kind_of => Integer, :default => 3
