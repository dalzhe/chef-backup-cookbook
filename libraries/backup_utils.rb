module BackupUtils
  def model_filename
    ::File.join(new_resource.directory, 'models', new_resource.name + '.rb')
  end

  def model_file(model_content)
    file model_filename do
      content model_content
      user    new_resource.user
      group   new_resource.group
      mode    0600
    end
  end

  def install_cron
    return if new_resource.cron.nil?

    c = new_resource.cron

    cron "run_#{new_resource.name}" do
      action      :create
      command     "backup perform --trigger '#{trigger}' --root-path #{new_resource.directory}"
      user        new_resource.user
      minute      c[:minute]   if c[:minute]
      hour        c[:hour]     if c[:hour]
      mailto      c[:mailto]   if c[:mailto]
      path        c[:path]     if c[:path]
    end
  end

  def trigger
    new_resource.name.gsub(' ', '_')
  end
end
