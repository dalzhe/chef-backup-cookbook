#
# Cookbook Name:: backup
# Provider:: s3_sync
#
# Author:: Joe Yates <info@bidsnc.com>
#
# Copyright 2015, Joe Yates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include BackupUtils

action :create do
  model_file(model_content)
  install_cron
end

private

def model_content
  <<-EOT
Backup::Model.new(:#{trigger}, 'Store backup file on S3') do
  archive :#{trigger} do |archive| 
    #{directory_content}
  end

  compress_with Gzip

  store_with S3 do |s3|
    s3.access_key_id     = "#{s3[:access_key]}"
    s3.secret_access_key = "#{s3[:secret_key]}"

    s3.bucket            = "#{s3[:bucket]}"
    s3.region            = "#{s3[:region]}"
    s3.path              = "#{s3[:path]}"
    s3.keep              = "#{new_resource.retention}"
  end
end
  EOT
end

def s3
  new_resource.s3_credentials
end

def directory_content
  new_resource.store.map { |d| "archive.add \"#{d}\"" }.join("\n") + "\n" +
  new_resource.exclude.map { |d| "archive.exclude \"#{d}\"" }.join("\n")
end
